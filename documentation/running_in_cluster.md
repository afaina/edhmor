# Running Edhmor in a cluster

## Installation 

### Install CoppeliaSim

CoppeliaSim is only available for Ubuntu in Linux, install the appropriate version for your cluster. If your cluster uses a different distribution, you can use singularity to run the Ubuntu version. If so, use the version for Ubuntu 18 (the singularity container that we have uses that version of Ubuntu).

```
cd
mkdir CoppeliaSim
##DOWNLOAD COPPELIASIM TO THIS FOLDER
tar -xvf CoppeliaSim_Edu_V4_4_0_rev0_Ubuntu18_04.tar.xz
```

Now, we need to copy the edhmor´s models and scenes to their respective folders (COPPELIASIM_HOME/scenes and COPPELIASIM_HOME/models).

### Install java

### Install MPJ

### Configure your bashrc profile

Add the following lines at the end of the .bashrc file in your home 

```
#load the singularity module (only necessary if you are using singularity)
module load singularity

#This avoids to use a virtual X server, like xvfb-run (it may not be necessary in CoppeliaSim 4.4.0)
export QT_QPA_PLATFORM=offscreen

#Choose JAVA version
export JAVA_HOME=/home/anfv/java/jdk-13.0.1/bin
#export JAVA_HOME=/home/anfv/java/jdk1.8.0_231/bin

#Define MPJ installation folder
export MPJ_HOME=/home/anfv/MPJ/mpj-v0_44

#Update the PATH to find Java and MPJ
export PATH="/home/anfv/tests/lib:$JAVA_HOME:$MPJ_HOME/bin:$MPJ_HOME/lib:$PATH"

#Update Library path to find the remoteApi
export COPPELIASIM_HOME=/home/anfv/VREP/CoppeliaSim_Edu_V4_4_0_rev0_Ubuntu18_04
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$COPPELIASIM_HOME/programming/legacyRemoteApi/remoteApiBindings/java/lib/Ubuntu18_04:$COPPELIASIM_HOME
```

## Running it 

To run the program, you need in the same folder:

1. a jar file with the java code (e.g., Edhmor.jar)
2. a lib folder containing all the required lib
3. simulatrionControl.xml and the jeaf control file

To run it, use the following command (replace $tasks with the number of simulators running in parallel):

```
mpjrun.sh -np $tasks -dev niodev -Xmx5000M -jar Edhmor.jar
```

After the evolution is finished, the output of the evolution can be found in the log folder.

## Running it in SLURM

If you are using slurm, create a slurm.mpj file in the same directory as the code, with the following text:

```
#!/bin/bash
#the name of your job on the SLURM system
#SBATCH -J ga_test_mpi
# the partition that you will use, the example here use the partiton called "cpu"
# please use sinfo command to check the available queues
#Avalaible options: cpu, gpu or desktop.
#SBATCH -p brown
#SBATCH --exclude=desktop17
# the system output and error message output, %J will show as your jobID
#SBATCH -o %J.out
#SBATCH -e %J.err
# the number of processors that you will use (in this example a 36), this is equal to ntasks
#SBATCH -n 8
#SBATCH --nodes=1                # Total number of nodes requested
# the wall time limit dor the script DD-HH:MM:SS
#SBATCH -t 2:20:00



############ enter your working directory, change to the location of where the executable (compiled program) is ###
#work_dir="/home/$USER/mpj"
#cd $work_dir

############ create machines file, DO NOT modify this section unless you really know ####
srun hostname|sort -u >machines
echo $SLURM_JOB_NODELIST
echo $SLURM_NTASKS
echo "SLURM_ARRAY_TASK_ID "
echo $SLURM_ARRAY_TASK_ID
mpjboot machines


############  Replace Foo with the name of your class that has main() method ###
tasks=$((SLURM_NTASKS*2))
echo "Number of tasks: "
echo $tasks
mpjrun.sh -np $tasks -dev niodev -Xmx5000M -jar Edhmor.jar

############ exit the mpd ring and clean off the nodes ###################
mpjhalt machines
`rm machines`
```

In this file, you need to configure the number of nodes, queue, max running time, etc. appropriately.

In addition, we recommend to create launching script that removes any previous data from the folder. Create a file launch_mpj.sh with the following text.

```
#!/bin/bash
rm -rf log/ *.err *.out machines results.txt symmetry_debug mpjrun.log* coppeliaSim_output
sbatch slurm.mpj
```

Give running permissions to the launch_mpj.sh file, and now you can run the evolution by simply running:

```
./launch_mpj.sh
```

You can check the id of the job and its status by using ´squeue -u <username>´ or cancel it with ´scancel <jobid>´

## Running several evolutions in SLURM

Create a folder called 1 with all the code ready to run and go to the parent folder. There, create a file called launch_experiments.sh  with the following text:

```
#!/bin/bash

echo "This will launch several evolutions based on folder 1. Introduce the initial folder number (e.g. 2)"
read firstFolder
echo "Introduce the last folder number (e.g 10)"
read lastFolder


#cd 1
#source launch_mpj.sh
#cd ..

for ((var=$firstFolder;var<=$lastFolder;var++))
do
    cp -r 1 $var
    cd $var
    source launch_mpj.sh
    cd ..
done
```
And run several evolutions by calling launch_experiments.sh and giving the parameters.

Sometimes, there are unexpected errors. You can check the status of all the evolutions by creating an script called analyze-results.sh:

```
#!/bin/bash

for d in */ ; do
    echo " "
    echo "$d"
    cd $d

    #Calculate number of errors
    if test -f *.out; then
        errors=$(grep -c "error" *.out)
        evals=$(grep -c "Evaluation" *.out)
        if test -f results.txt; then
            echo "It run withot problems. $errors errors and $evals evaluations."
        else
            echo "$errors errors and $evals evaluations until now."
        fi
    else
        echo "Not running..."
    fi

    ##source stats.sh
    cd ..
done

#FILE=/etc/resolv.conf
#if test -f "$FILE"; then
#    echo "$FILE exist"
#fi
```

