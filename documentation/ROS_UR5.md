# UR5 ROS Control Programs to install in Ubuntu 18.04

## Setup 

- ROS Melodic http://wiki.ros.org/melodic/Installation/Ubuntu
- Universal Robots ROS driver https://github.com/UniversalRobots/Universal_Robots_ROS_Driver - Locate calibration file as per in instructions
- Move it for ROS melodic http://docs.ros.org/en/melodic/api/moveit_tutorials/html/index.html
- Ros bridge suite http://wiki.ros.org/rosbridge_suite
- move the testing_ur5 folder to the src folder in your catkin workspace
- move the ur_description, ur_gazebo and ur5_mag_eef_moveit_config folders to the fmauch_universal_robot folder in the src folder in your catkin workspace and run catkin_make
- if you downloaded the files in windows, change the file endings to unix format. In Vim, you can use the command `:set ff=unix`
- run `chmod +x` for move_server_poseArray.py, fake_force_sensors_publisher.py and vrep_joint_subscriber.py inside testing_ur5/scripts/
- update the environment 

```cd ~/catkin_ws
source devel/setup.bash
```

## Launching 

Launch each of these in a separate terminal window, remember to source: source devel/setup.bash in the catkin workspace you have set up for all terminals you use  

### Simulated Robot with Rviz only (No connection to Java, for testing the planning algorithms)

- Simulation:       `roslaunch ur_gazebo ur5_bringup.launch`
- Moveit commander: `roslaunch ur5_mag_eef_moveit_config ur5_mag_eef_moveit_planning_execution_rviz.launch sim:=true limited:=true`

### Simulated Robot [Step by step]

- Simulation:       `roslaunch ur_gazebo ur5_bringup.launch`
- Moveit commander: `roslaunch ur5_mag_eef_moveit_config ur5_mag_eef_moveit_planning_execution.launch sim:=true limited:=true`
- Rosbridge:        `roslaunch rosbridge_server rosbridge_websocket.launch`
- Fake force sensor values generator: `rosrun testing_ur5 fake_force_sensors_publisher.py`
- Coppelia joint subscriber: `rosrun testing_ur5 vrep_joint_subscriber.py`
- Server for java:  `rosrun testing_ur5 move_server_poseArray.py`


### Simulated Robot [Easy way]

- Simulation:       `roslaunch ur_gazebo ur5_bringup.launch`
- Rest of things:   `roslaunch testing_ur5 simulator.launch`

### Real Robot

- Driver:           `roslaunch ur_robot_driver ur5_bringup.launch robot_ip:=192.168.56.10 kinematics_config:=/home/fai/my_robot_calibration.yaml`
- Moveit commander: `roslaunch ur5_mag_eef_moveit_config ur5_mag_eef_moveit_planning_execution_rviz.launch limited:=true`
- Rosbridge:        `roslaunch rosbridge_server rosbridge_websocket.launch`
- Server for java:  `rosrun testing_ur5 move_server_poseArray.py`

If you wish to see the robot movements in CoppeliaSim with either the simulated or real robot run:

- CoppeliaSim joint subscriber node: `rosrun testing_ur5 vrep_joint_subscriber.py` (A coppeliasim simulator with the appropriate model must be running for the node to work)

## Moving the robot

After launching everything you should be able to send cartesian paths to the (sim) robot using a ROS request, the request has two fields
    command: String command, has the following options: "home", makes the robot go to the home position (this is the first (arbitrary) position that the robot will move to when launching the python server) no waypoints needed (empty array), "pose": will attempt to plan and execute a pose_goal command with the first pose provided inside the waypoints array, since the OMPL planner is used (probabilistic planning) a valid plan may not be discovered in the first attempt, the server witll attempt 10 times before returning a failed response, this can be modified in the server response, "cartesian": will attempt to plan and execute a cartesian path with the waypoints provided, specifics can be further configured in the python server, 
    waypoints: Array of poses for the robot to make (Warning: if the robot is too far from the initial point the planning and execution will fail), for the "pose" command the array must contain at least one pose. 

The response has three fields:

- suceess: boolean, indicates if the planned path or command was executed successfully
- fraction: float, the fraction of the carthesian path that is followed by the planned movements, remains 0.0 for a "pose" command response
- message: string, a meesage from the server related to the planning and execution attempt

## Notes

- The initial home position critically affects the ability of the planner to calculate a plan, right now best results have been obtained when the robot's front is facing the positve or negative x axis, this keeps the initial angles of the robot's joints below +-pi.
- **Attention!!** When using the "pose" command the robot may take any trajectory in joint space (that doesn't collide with the obstacles known by the planner) to get to the goal, this means movements can potentially go behind the robot base and make multiple turns in some of the joints. This is specially true for eef orientation changes!!, take care and use only for simple translation movements when the path is not critical, prefferably use the "cartesian" command for eef orientation changes or when the path is critical. Check the MoveitTest class for a minimal example.  


