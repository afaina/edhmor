# VISP

We use [Visp](https://visp.inria.fr/) to find [AprilTags](https://april.eecs.umich.edu/software/apriltag) in the image from the cameras and get their positions and orientations. This only needs to be installed if you want to perform real tests with hardware (or simulating the real hardware setup). 

## Installing VISP

Follow the [Installation from source for Windows with Visual C++ 2019 (vc16)](https://visp-doc.inria.fr/doxygen/visp-daily/tutorial-install-win10-msvc16.html)

* Install all prerequisites (from third parties, only OpenCV is neccessary)
* After downloading the source code from git (**and before running cmake!!**) replace the file C:\visp-ws\visp\modules\java\misc\core\gen_dict.json for the one in the visp folder, replace also the filelist file in the same folder
* After installing visp normally follow the section https://visp-doc.inria.fr/doxygen/visp-daily/tutorial-install-java.html#set_up_visp_java_eclipse to setup the jar and external libraries in eclipse


## Using Big AprilTag Families

To use big AprilTags (TAG_CIRCLE49h12, TAG_CUSTOM48h12, TAG_STANDARD52h13, TAG_STANDARD41h12), the flag ´VISP_HAVE_APRILTAG_BIG_FAMILY´ should be enabled before compiling it. You can enable it in CMAKE and then compile normally. 

While this enough to use them, detecting for tags takes very long and consumes a lot of memory. We recommend to modify visp/modules/detection/src/tag/vpDetectorAprilTag.cpp in the following way:

The two lines ´apriltag_detector_add_family(m_td, m_tf);´ should be replaced by:

```
if(m_tagFamily == TAG_CUSTOM48h12)
	apriltag_detector_add_family_bits(m_td, m_tf, 1);
else
    apriltag_detector_add_family(m_td, m_tf);
```

Of course, this code only makes faster the TAG_CUSTOM48h12 family. 
