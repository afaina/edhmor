# Evolutionary Designer of Heterogeneous Modular Robots (EDHMOR) #

[![Alt text for your video](https://j.gifs.com/yD8wAG.gif)](http://www.youtube.com/watch?v=71596tvtp_M) [![Alt text for your video](https://j.gifs.com/KdLAze.gif)](http://www.youtube.com/watch?v=ekhvwQANzPY)

How is it possible to automatically design a robot for a task and build it less than one hour? 
 
To reach this goal, the EDHMOR system uses modular robots and evolutionary algorithms. Modular robots are simple and autonomous robotic devices that can be attached together. They provide simple building blocks with sensing, computational and actuation capabilities that allow us to deploy complex robots in minutes. Modular robots have been tested in a lot of different tasks, but usually the design of the robot (how the modules are assembled between them) is selected by an expert user. In contrast, the EDHMOR system employs evolutionary algorithms to find a suitable design, also called morphology or configuration, which can be built in a few minutes by assembling the modules. This repository contains all the required files to evolve the morphology of the modular robot and the parameters of the controller for a specific task.

The main features of EDHMOR are:

* Code written in Java, multi-platform
* Supports homogeneous and heterogeneous modules:
  * EDHMOR modules (heterogeneous)
  * Emerge modules (homogeneous)
* Modules can only have one degree of freedom (but we are working to remove this limitation)
* Evolutionary algorithms are provided by JEAF library
* Evaluation of the robot in CoppeliaSim simulator
* Control based on sinusoidal controllers
* Distributed evaluation of the individuals
* Able to perform real hardware experiments with a physical setup (using cameras and an industrial manipulator)

Most of the current experiments are carried our using [Emerge modules](https://www.frontiersin.org/articles/10.3389/frobt.2021.699814/full). They are perfect to build evolved robots as they are:

* Easy to build (assembling time is half an hour)
* Easy to deploy (manually or with a robot manipulator)
* Robust (magnetic connectors will detach if the force is excessive without damaging the modules)
* Easy localization of the modules (connectors can be tracked with AprilTags)

## Evolving modular robots ##

### Installation 

EDHMOR uses [JAVA Evolutionary Algorithm Framework (JEAF)](https://github.com/failiz/JEAF) and a few other libraries. All the needed libraries to evolve robots are included in the edhmor\lib folder. Visp library needs to be installed to locate Apriltags, but it is not necessary to evolve modular robots in simulation. If your IDE warns you about this, just ignore it.  

Next, let´s install the simulator.


#### Simulator - [CoppeliaSim](http://www.coppeliarobotics.com/) ####

EDHMOR uses a simulator to evaluate the morphology and the control parameters of the robots. Currently, only the CoppeliaSim simulator is supported. We used Gazebo 0.8 for the first publications, and the documentation for setting up this legacy simulator can be found [here](documentation/gazebo-0.8.md). CoppeliaSim adds new features in the simulation such as force sensors in all the connections and collisions between the modules of the robot. It increases the realism of the simulation and help us to reduce the reality gap between the simulations and the real robot.

The models of the modules for the CoppeliaSim simulator can be found in `edhmor\models`. In order to provide these models to CoppeliaSim, copy them into `COPPELIASIM_INSTALLATION_PATH\models`. It is also necessary to copy the scenes from `edhmor\scenes` into `COPPELIASIM_INSTALLATION_PATH\scenes`.

The communications between the Java code and the simulator is achieved through sockets. In order to be able to communicate, CoppeliaSim has to be configured. Check the file remoteApiConnections.txt in the CoppeliaSim folder, the three variables inside this file have to be set to: 

```
portIndex1_port 		= 19997
portIndex1_debug 		= false
portIndex1_syncSimTrigger = true
```

** macOS: The remoteApiConnections.txt does not work in macOs. Type `simRemoteApi.start(19997,1300,false,true)`  in the Lua commander to activate the server. **

In addition, the java program has to be able to find the CoppeliaSim api library. This can be done by adding the path of the library to the environmental variable, LD_LIBRARY_PATH in linux or PATH in Windows. Alternatively, edit the project properties in the run menu of your IDE by writing this line in the VM options:

```
-Djava.library.path=PATH_TO_THE_LIBRARY
```

As an example:

```
-Djava.library.path="C:\Program Files\CoppeliaRobotics\CoppeliaSimEdu\programming\remoteApiBindings\java\lib\64Bit"
```

#### Configuration Files

There are two configuration files that need to be setup. The first one, simulationControl.xml, specifies the parameters of the robots (type of modules, controller, environment, etc.). This file needs to be in the same folder where the src folder is. This file is not tracked as is changed frequently, but there is a template in the repository. You should make a copy of `SimulationControlTemplate.xml` and rename it to `simulationControl.xml`. 

There is another configuration file which setups the evolutionary algorithm to use. The file of the evolutionary algorithm parameters to use is chosen in simulationControl.xml. There are several evolutionary algorithms that you can use but we have a few templates in the jeaf_conf_files folder. These configuration files are used by the JEAF library to perform the evolutionary algorithm. 

#### Testing the connection with CoppeliaSim

After installing the simulator and preparing the configuration files, we can check that everything is working as expected. To do that, first, launch CoppeliaSim. After it has finished loading, you can run the following file: `tests.coppeliasim.EvaluateRandomRobot.java`

If everything is correct, you should see how a random robot is assembled, the simulation is performed, and the fitness is displayed.

### Evolving robots with serial evaluation

First, you should setup the configuration files to perform your experiment. After that, simply run the file `modules.jeaf.application.edhmor.MainClass.java`

If CoppeliaSim is open, you will see the evaluations of the robots. If the simulator is not opened, the software will create one without the GUI. If you kill the java program before it ends, you have to kill the simulator with the task manager. The default template of simulationControl.xml will run EDHMOR (evaluations: 25000, population: 40) using a serial evaluation. 

### Evolving robots with parallel evaluation

This is very convenient as simulations take a while to finish. We use a cluster of computers to simulate all the population in a short time. To use this feature, you need to:

1. Adjust the evolutionary algorithm configuration file to use a parallel evaluation (see templates such as jeaf_conf_files/edhmor_parallel.xml)
2. Set UseMPI parameter to true in simulationControl.xml
3. Install [MJP Express](http://mpjexpress.org/)
4. Run `modules.jeaf.application.edhmor.MainClass.java`

You can find more information about how to run Edhmor in a cluster in [this section](documentation/running_in_cluster.md).

### Info for developers

The main class, `MainClass.java`, sets up the parameters of the evolutionary algorithm by reading a configuration file, see jeaf_conf_files folder. This file stores all the parameters related to the evolutionary algorithm and has to be properly configured. The parameters related to modules, task to perform, environmental conditions and evaluation of the candidates are set in simulationControl.xml. simulationControl.xml should be placed in the same folder as the executable (\*.jar) file. In addition, all the libraries should be placed in a folder called lib.  

The evaluation is performed using the class CoppeliaSimEvaluator.java. It creates a CoppeliaSimCreateRobot instance, which loads the modules and the force sensors in CoppeliaSim in the correct location. Then, CoppeliaSimEvaluator starts a loop where it checks the limit of the force sensors, sets the goal position of the motor for all the modules and triggers a new run of the physics engine. This loop is executed until the simulation time is reached. Finally, the position of all the modules is acquired and a fitness value is generated.

For running simple tests, it is necessary to start the CoppeliaSim simulator manually. You can start it without its Graphical User interface for faster simulations or with it if you want to see the simulations while they are running. When evolving robots, the program tries to connect to CoppeliaSimulators already running. If this fails, the program automatically starts the CoppeliaSim simulators. In case of parallel evaluations, the simulators are open in different ports to allow having several simulators in the same machine. 

### Graphical User Interface ###

The TestGUI.java class is a graphical tool to see the solutions found by the evolutionary algorithm. These solutions are stored in log files and each solution is a string of numbers. First, run the TestGUI program and copy and paste the solution in text field called “Individual”. Then, select the right parameters for modules, simulators and other environments and press run. The evaluation of the morphology and the control parameters will be performed in the selected simulator and the fitness or score of the solution will be displayed when the evaluation finishes. In addition, this tool also allows removing or adding modules to one robot. Finally, it can also display the relations between the modules as it can plot a robot as a tree, where each module is a node of the tree, in order to analyze how the robot is built.     

## Working with real hardware ##

[![Alt text for your video](https://j.gifs.com/830QA3.gif)](https://vimeo.com/540894612) [![Alt text for your video](https://j.gifs.com/k2NqK6.gif)](https://vimeo.com/540894612)

Currently, we are working to evolve modular robots without a simulator. In order to do so, you need to install some additional libraries:

* [Dynamixel SDK](https://github.com/ROBOTIS-GIT/DynamixelSDK)
* [Visp](documentation/VISP.md)

## Evaluating real robots 

The evolved robots can be deployed in seconds as they modules are already manufactured. Emerge modules can be assembled in one or two minutes. A GUI allows to control the assembling and the evaluation process, which is available at `modules.gui.RealWorldTestGUI.java`. 

### Assembling the robot

In this GUI, you can observe how to assemble the robot in CoppeliaSim module by module and how the robot works in simulation. In addition, we have made a small program that uses a handheld camera to assist you in the assembling process. This program shows how to build the evolved robot module by module and checks that the assembled module is in the right place and with the right orientation. 

### Evaluating the robot

After the robot is assembled, it is possible to move the servos of the modules. Start by moving to the first position softly and then check that the robot moves as in the simulation by controlling the "time" slider. After you have checked that the robot is correctly assembled, you can evaluate it in the real world. For this, a camera is used to track the Apriltag of the base in order to calculate the fitness. 


## References ##

This system evolves morphologies and controllers for a specific task using a heterogeneous modular system. The specifications of the heterogeneous modular system and an ad-hoc algorithm to deal with the deceptive search space of this problem can be found in the following papers:


Faiña, A., Bellas, F., Orjales, F., Souto, D., & Duro, R. J. (2015). [An evolution friendly modular architecture to produce feasible robots.](https://www.researchgate.net/publication/265052601_An_evolution_friendly_modular_architecture_to_produce_feasible_robots) Robotics and Autonomous Systems, 63, 195-205.

Faíña, A., Bellas, F., López-Peña, F., & Duro, R. J. (2013). [EDHMoR: Evolutionary designer of heterogeneous modular robots.](http://www.sciencedirect.com/science/article/pii/S0952197613001838) Engineering Applications of Artificial Intelligence, 26(10), 2408-2423.

Faíña, A., Souto, D., Orjales, F., Bellas, F., & Duro, R. J. (2014). [Towards feasible virtual creatures by using modular robots.](https://www.researchgate.net/publication/267326645_Towards_feasible_virtual_creatures_by_using_modular_robots) In Modular and Swarm Systems. IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS).

Moreno, Rodrigo, and Andres Faiña. [EMERGE Modular Robot: A Tool for Fast Deployment of Evolved Robots.](https://doi.org/10.3389/frobt.2021.699814) Frontiers in Robotics and AI (2021): 198.

## Videos ##

The videos showing the evolved robots can be found in the following links:

http://vimeo.com/afaina/feasible-virtual-creatures

https://vimeo.com/afaina/emerge-evolved-robots